# GitLab DevSecOps - Workshop Notes Application

This application is used along with the GitLab DevSecOps workshop found
at https://tech-marketing.gitlab.io/devsecops/devsecops-workshop/workshop/

It is not meant to be used directly, but rather to be cloned and used in
your environment and deployed to a Kubernetes Cluster.

---

Enjoy! Contact @fjdiaz for any information around this Workshop.
